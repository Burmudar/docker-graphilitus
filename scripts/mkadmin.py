import os, sys
sys.path.append('/opt/graphite/webapp/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "graphite.local_settings")

#Needed otherwise django throws "AppRegistryNotReady"
#import django
#django.setup()

try:
    #Try using the new method of getting the user model
    from django.contrib.auth import get_user_model
    User = get_user_model()
except Exception,ImportError:
    from django.contrib.auth.models import User

    user, created = User.objects.get_or_create(username='admin')
    if created:
        user.set_password('flickswitch')
        user.is_superuser = True
        user.is_staff = True
        user.save()
