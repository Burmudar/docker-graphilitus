#!/bin/bash
cd /opt/graphite/webapp/graphite && gunicorn --bind=127.0.0.1:8080 --preload --workers=3 --pid=/var/run/gunicorn-graphite.pid --debug --log-leve=DEBUG graphite_wsgi:application
