FROM ubuntu:14.04

RUN apt-get update && apt-get install -y build-essential python-setuptools python-pip python-cairo python-dev curl wget python-mysqldb supervisor gunicorn nginx
RUN pip install whisper carbon graphite-web django==1.5 django-tagging twisted==11.1.0 && apt-get autoclean

ADD scripts/ /root/scripts/
#ADD graphite/whisper/ /opt/graphite/storage/whisper/
ADD graphite/conf/ /opt/graphite/conf/
ADD nginx/conf/ /etc/nginx/sites-available/
ADD graphite/local_settings.py /opt/graphite/webapp/graphite/local_settings.py
ADD graphite/graphite_wsgi.py /opt/graphite/webapp/graphite/graphite_wsgi.py

RUN python /opt/graphite/webapp/graphite/manage.py syncdb --noinput && python ~/scripts/mkadmin.py && chown -R www-data:www-data /opt/graphite/ && ln -s /etc/nginx/sites-available/base.conf /etc/nginx/sites-enabled/base.conf && echo 'daemon off;' >> /etc/nginx/nginx.conf && rm /etc/nginx/sites-available/default

RUN curl -L http://nodejs.org/dist/v0.10.20/node-v0.10.20-linux-x64.tar.gz | tar xz -C /opt/ && mv /opt/node-v0.10.20-linux-x64 /opt/nodejs 
ENV PATH /opt/nodejs/bin/:$PATH
RUN npm -g install statsd
ADD statsd/ /opt/statsd/

ADD supervisord/conf/ /etc/supervisor/conf.d/


EXPOSE 8125 8080 9001

CMD /usr/bin/supervisord
